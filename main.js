export function setup(ctx) {
    const id = 'semi-auto-collect-stockpiles';
    const title = 'SEMI Auto Collect Stockpiles';
    const desc = "Collects your passive food from stockpiles as soon as it cooks.";
    const imgSrc = 'assets/media/bank/rune_blood.png';

    ctx.patch(Cooking, 'addItemToStockpile').after((result, category, itemID, quantity) => {
        game.cooking.onCollectStockpileClick(category);
    })

    mod.api.SEMI.log(id, 'Successfully loaded!')
}
